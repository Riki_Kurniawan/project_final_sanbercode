@extends('admin.master')
@section('content')
@section('Judul-Page','KATEGORI BOS')


@if(Session::has('success'))
<div class="alert alert-success" role="alert">
   {{Session('success')}}
   </div>
@endif

<a href="{{route('category.create')}}" class="btn btn-info btn-sm">Tambah Kategori</a>
<table class="table tablle-striped table-hover table-sm table-bordered table-da">
  <thead>
    <tr>
      <th>Id</th>
      <th>Nama</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($category as $result => $hasil)
    <tr>
      <td>{{$result+$category->firstitem()}}</td>
      <td>{{$hasil->nama}}</td>
      <td>
          <form action="{{route('category.destroy', $hasil->id)}}" method="POST">
              @csrf
              @method('delete')
              <a href="{{ route('category.edit', $hasil->id)}}" class="btn btn-primary">Edit</a>
          <button type="submit" class="btn btn-danger">Delete</button>
          </form>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
{{ $category->links() }}
@endsection
