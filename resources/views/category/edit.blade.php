@extends('admin.master')
@section('content')
@section('Judul-Page','Edit Category')

@if (count($errors)>0)
    @foreach($errors->all() as $error)
    <div class="alert alert-danger" role="alert">
    {{$error}}
    </div>
    @endforeach
@endif

@if(Session::has('success'))
<div class="alert alert-success" role="alert">
   {{Session('success')}}
   </div>
@endif
<form action="{{route('category.update', $category->id)}}" method="POST">
@csrf
@method('patch')
<div class="form-group">
    <label for="">Kategori</label>
    <input type="text" class="form-control" name="nama" value="{{$category->nama}}">
</div>
<div class="form-group">
    <button class="btn btn-primary btn-block">UPDATE DATA</button>
</div>
</form>

<a href="{{route('category.index')}}" class="btn btn-success">Kembali</a>
@endsection
