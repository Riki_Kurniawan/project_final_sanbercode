@extends('admin.master')
@section('content')
@section('Judul-Page','Keranjang Sampah [Postingan]')


@if(Session::has('success'))
<div class="alert alert-success" role="alert">
   {{Session('success')}}
   </div>
@endif

<table class="table table-striped table-hover table-sm table-bordered table-da">
  <thead>
    <tr>
      <th>No</th>
      <th>Judul</th>
      <th>Category</th>
      <th>Tags</th>
      <th>Image</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($postdelete as $result => $hasil)
    <tr>
      <td>{{$result+$postdelete->firstitem()}}</td>
      <td>{{$hasil->judul}}</td>
      <td>{{$hasil->category->nama}}</td>
      <td>@foreach ($hasil->tags as $tag)
        <a href="" class="btn btn-success btn-sm">{{$tag->nama}}</a>
          @endforeach
      </td>
      <td><img src="{{asset($hasil->gambar)}}" alt="" class="img-fluid" style="width:80px"></td></td>
      <td>
          <form action="{{route('post.kill', $hasil->id)}}" method="POST">
              @csrf
              @method('delete')
              <a href="{{route('post.restore',$hasil->id)}}" class="btn btn-info">Restore</a>
              <button type="submit" class=" btn btn-danger">Delete</button>
          </form>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>

{{ $postdelete->links() }}
@endsection
