@extends('admin.master')
@section('content')
@section('Judul-Page','POSTINGAN')


@if(Session::has('success'))
<div class="alert alert-success" role="alert">
   {{Session('success')}}
   </div>
@endif

<a href="{{route('post.create')}}" class="btn btn-info btn-sm">Tambah POST</a>
<table class="table tablle-striped table-hover table-sm table-bordered table-da">
  <thead>
    <tr>
      <th>No</th>
      <th>Judul</th>
      <th>Category</th>
      <th>Tags</th>
      <th>Image</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($post as $result => $hasil)
    <tr>
      <td>{{$result+$post->firstitem()}}</td>
      <td>{{$hasil->judul}}</td>
      <td>{{$hasil->category->nama}}</td>
      <td>@foreach ($hasil->tags as $tag)
        <a href="" class="btn btn-success btn-sm">{{$tag->nama}}</a>
          @endforeach
      </td>
      <td><img src="{{asset($hasil->gambar)}}" alt="" class="img-fluid" style="width:80px"></td></td>
      <td>
          <form action="{{route('post.destroy', $hasil->id)}}" method="POST">
              @csrf
              @method('delete')
              <a href="{{ route('post.edit', $hasil->id)}}" class="btn btn-primary fas fa-pen"></a>
          <button type="submit" class=" btn btn-danger fas fa-trash"></button>
          </form>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>

{{ $post->links() }}
@endsection
