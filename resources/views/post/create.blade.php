@extends('admin.master')
@section('content')
@section('Judul-Page','Create POST')

@if (count($errors)>0)
    @foreach($errors->all() as $error)
    <div class="alert alert-danger" role="alert">
    {{$error}}
    </div>
    @endforeach
@endif

@if(Session::has('success'))
<div class="alert alert-success" role="alert">
   {{Session('success')}}
   </div>
@endif

<form action="{{route('post.store')}}" method="POST" enctype="multipart/form-data">
@csrf
<div class="form-group">
    <label for="">Judul</label>
    <input type="text" class="form-control" name="judul">
</div>

<div class="form-group">
    <label for="">Kategori</label>
    <select class="form-control" name=category_id>
    <option value="">Pilih Category</option>
    @foreach($category as $hasil)
        <option value="{{$hasil->id}}">{{$hasil->nama}}</option>
    @endforeach
    </select>
</div>


<div class="form-group">
    <label for="">Pilih Tag</label>
    <select class="form-control select2" multiple=""  name="tags[]">
        @foreach ($tags as $tag)
        <option value="{{$tag->id}}">{{$tag->nama}}</option>
        @endforeach
    </select>
</div>


<div class="form-group">
    <label for="">Isi Content</label>
    <input type="text" class="form-control" name="content">
</div>

<div class="form-group">
    <label for="">Thumbnail</label>
    <input type="file" class="form-control" name="gambar">
</div>
<div class="form-group">
    <button class="btn btn-primary btn-block">SIMPAN</button>
</div>
</form>

<a href="{{route('post.index')}}" class="btn btn-success">Kembali</a>


@endsection
