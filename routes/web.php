<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\PostController;

Route::get('/', function () {
    return view('welcome');
});

Route::resource('/category', 'CategoryController');
Route::resource('/tag', 'TagController');
Route::get('/post/hapus', 'PostController@list_postdelete')->name('list_postdelete');
Route::get('/post/restore{id} ', 'PostController@restore')->name('post.restore');
route::delete('/post/Kill{id}', 'PostController@kill')->name('post.kill');
Route::resource('post','PostController');

Route::get('/dashboard', function(){
    return view('admin.master');
})->name('dashboard');

