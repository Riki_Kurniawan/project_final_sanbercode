<?php

namespace App\Http\Controllers;
use App\Tags;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tag = Tags::paginate(10);
        return view('tag.index',compact('tag'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tag.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //menambahkan validasi nama -> wajib min.3 karakter
        $this->validate($request,[
            'nama' => 'required|min:3'
        ]);
        Tags::create([
            'nama' => $request->nama,
            'slug' => Str::slug($request->nama)
        ]);
        return redirect()->back()->with('success','Data berhasil  disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $tag = Tags::findorfail($id);
        return view('tag.edit', compact('tag'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama' => 'required'
        ]);
        $tag_data = [
        'nama' => $request->nama,
        'slug' => Str::slug($request->nama)
        ];
        Tags::whereId($id)->update($tag_data);
        return redirect()->route('tag.index')->with('success','Tag berhasil  diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $tag = Tags::findorfail($id);
        $tag->delete();
        return  redirect()->back()->with('success','Tag berhasil dihapus');
    }
}
