<?php

namespace App\Http\Controllers;
use App\Posts;
use App\Category;
use App\Tags;
use Illuminate\Support\Str;

use Illuminate\Http\Request;
class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post = Posts::paginate(10);
        return view('post.index', compact('post'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = Tags::all();
        $category = Category::all();
        return view('post.create', compact('category','tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
           'judul' => 'required ',
           'category_id' =>  'required',
           'content' => 'required',
           'gambar' => 'required'
        ]);

        $gambar = $request->gambar;
        // menghindari redudansi data :  Unik nama gambar
        $nama_gambar = time().$gambar->getClientOriginalName();

        $post = Posts::create([
           'judul' => $request->judul,
           'category_id' =>  $request->category_id,
           'content' => $request->content,
           'gambar' =>  'public/uploads/posts/'.$nama_gambar,
           'slug' => Str::slug($request->judul)
        ]);
            //penyimpanan multiple select : tags
            $post->tags()->attach($request->tags);
            //lokasi penyimpanan file gambar
            $gambar->move('public/uploads/posts/', $nama_gambar);
            return redirect()->route('post.index')->with('success', 'Postingan anda berhasil disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::all();
        $tags = Tags::all();
        $post = Posts::findorfail($id);
        return view('post.editpost', compact('post','tags','category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'judul' => 'required',
            'category_id' => 'required',
            'content' => 'required'
         ]);

        $post = Posts::findorfail($id);

        //jika terdapat gambar disertakan
        if ($request->hasFile('gambar')) {
            $gambar = $request->gambar;
            $nama_gambar = time().$gambar->getClientOriginalName();
            $gambar->move('public/uploads/posts/', $nama_gambar);

        $post_data = [
            'judul' => $request->judul,
            'category_id' =>  $request->category_id,
            'content' =>  $request->content,
            'gambar' => 'public/uploads/posts/'.$nama_gambar,
            'slug' => Str::slug($request->judul)
        ];
        }


        //jika tidak terdapat gambar disertakan
        else{
        $post_data = [
            'judul' => $request->judul,
            'category_id' =>  $request->category_id,
            'content' =>  $request->content,
            'slug' => Str::slug($request->judul)
        ];
        }

        $post->tags()->sync($request->tags);
        $post->update($post_data);

        return redirect()->route('post.index')->with('success','Postingan anda berhasil diupdate');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Posts::findorfail($id);
        $post->delete();
        return redirect()->back()->with('success', 'Postingan berhasil di delete');

    }

    public function list_postdelete(){
        $postdelete  = Posts::onlyTrashed()->paginate(10);
        return view ('post.deletepost', compact('postdelete'));
    }

    public function restore($id){
        $post_restore = Posts::withTrashed()->where('id', $id)->first();
        $post_restore->restore();
        return redirect()->back()->with('success','Postingan berhasil di Restore');
    }

    public function kill($id){
        $post_kill = Posts::withTrashed()->where('id', $id)->first();
        $post_kill->forceDelete();

        return redirect()->back()->with('success', 'Postingan berhasil dihapus Permanen');



    }
}
