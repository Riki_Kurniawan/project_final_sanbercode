<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Posts extends Model
{
    use SoftDeletes;

    protected $date = ['delete_at'];
    protected $fillable = ['judul','category_id','content','gambar','slug'];

    public function category(){
        return $this->BelongsTo('App\Category');
    }

    public function tags(){
        return $this->belongsToMany('App\Tags');
    }
}
